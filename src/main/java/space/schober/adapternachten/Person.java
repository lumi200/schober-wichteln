package space.schober.adapternachten;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.validator.routines.DomainValidator;
import org.apache.commons.validator.routines.EmailValidator;

public class Person {
	private static Logger logger = App.logger;

	private Name name;
	private String email;
	private LocalDate birthday;
	private Person linkedPerson;
	private static Random rng = new Random();

	public Person(Name name, String email, LocalDate birthday) {
		this.setName(name);
		this.setEmail(email);
		this.setBirthday(birthday);
	}

	@Override
	public String toString() {
		StringBuilder stb = new StringBuilder();
		stb.append(String.format("name: %s, e-mail: %s, birthday: %s, linked person: [ ", this.getName(),
				this.getEmail(), this.getBirthdayString()));
		if (this.getLinkedPerson() != null)
			stb.append(this.getLinkedPerson().getName());
		else
			stb.append("none");
		stb.append(" ]");

		return stb.toString();
	}

	public static void addLinkedPersons(Set<Person> persons) {
		ArrayList<Person> personList = new ArrayList<>();
		personList.addAll(persons);

		Person newLinkedPerson;
		for (Person person : persons) {
			do {
				newLinkedPerson = personList.get(rng.nextInt(personList.size()));
				person.setLinkedPerson(newLinkedPerson);
				logger.log(Level.FINEST, "Looping...");
			} while (person.getLinkedPerson().equals(person));

			personList.remove(newLinkedPerson);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((birthday == null) ? 0 : birthday.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((linkedPerson == null) ? 0 : linkedPerson.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (birthday == null) {
			if (other.birthday != null)
				return false;
		} else if (!birthday.equals(other.birthday))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (linkedPerson == null) {
			if (other.linkedPerson != null)
				return false;
		} else if (!linkedPerson.equals(other.linkedPerson))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public static boolean isValidEmailAddress(String email) {
		boolean allowLocal = true;
		EmailValidator ev = new EmailValidator(allowLocal, true, DomainValidator.getInstance(allowLocal));
		return ev.isValid(email);
	}

	public Name getName() {
		return name;
	}

	public void setName(Name name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		if (isValidEmailAddress(email))
			this.email = email;
		else
			logger.log(Level.SEVERE, String.format("Invalid email! %s", email));
	}

	public LocalDate getBirthday() {
		return birthday;
	}

	public String getBirthdayString() {
		return birthday.toString();
	}

	public void setBirthday(LocalDate birthday) {
		this.birthday = birthday;
	}

	public Person getLinkedPerson() {
		return linkedPerson;
	}

	public void setLinkedPerson(Person linkedPerson) {
		this.linkedPerson = linkedPerson;
	}
}
