package space.schober.adapternachten;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class Email {
	private static Logger logger = App.logger;

	private final String username;
	private final String senderEmail;
	private final Properties properties = new Properties();
	private final Session session;
	private static final String ADAPTERNACHTEN = "Adapternachten";

	public Email(String username, String password) {
		this.username = username;
		this.senderEmail = this.username;

		properties.put("mail.smtp.host", "smtp.gmail.com"); // SMTP Host
		properties.put("mail.smtp.socketFactory.port", "465"); // SSL Port
		properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory"); // SSL Factory Class
		properties.put("mail.smtp.auth", "true"); // Enabling SMTP Authentication
		properties.put("mail.smtp.port", "465"); // SMTP Port

		// create Authenticator object to pass in Session.getInstance argument
		Authenticator auth = new Authenticator() {
			// override the getPasswordAuthentication method
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		};

		session = Session.getInstance(properties, auth);
	}

	public boolean sendEmail(Person receiver, String html, String srcPath, List<String> adapterList) {
		return sendEmail(receiver, ADAPTERNACHTEN, html, srcPath, adapterList);
	}

	private MimeMessage getBasicMimeMessage(String senderEmail, Person receiver, String subject)
			throws MessagingException, UnsupportedEncodingException {
		MimeMessage msg = new MimeMessage(this.session);
		// set message headers
		msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
		msg.addHeader("format", "flowed");
		msg.addHeader("Content-Transfer-Encoding", "8bit");

		msg.setFrom(new InternetAddress(senderEmail, ADAPTERNACHTEN));
		msg.setReplyTo(InternetAddress.parse(senderEmail, false));
		msg.setSentDate(new Date());
		msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(receiver.getEmail(), false));
		msg.setSubject(subject, "UTF-8");

		return msg;
	}

	private String insertDataintoHTML(String html, Person receiver) {
		return html.replace("%RECEIVER%", receiver.getLinkedPerson().getName().getCompleteName());
	}

	private boolean sendEmail(Person receiver, String subject, String htmlText, String srcPath,
			List<String> adapterList) {
		try {
			MimeMessage msg = getBasicMimeMessage(senderEmail, receiver, subject);

			MimeMultipart multipart = new MimeMultipart("related");
			MimeBodyPart messageBodyPart = new MimeBodyPart();

			// Add HTML
			messageBodyPart.setText(insertDataintoHTML(htmlText, receiver), "UTF-8", "html");
			multipart.addBodyPart(messageBodyPart);

			// Add header GIF
			messageBodyPart = new MimeBodyPart();
			DataSource fds = new FileDataSource(srcPath + "Header/header.gif");
			messageBodyPart.setDataHandler(new DataHandler(fds));
			messageBodyPart.setHeader("Content-ID", "<header>");
			multipart.addBodyPart(messageBodyPart);

			// Add all adapter (which are flying around in the background)
			int count = 0;
			for (String adapter : adapterList) {
				messageBodyPart = new MimeBodyPart();
				fds = new FileDataSource(srcPath + adapter);
				messageBodyPart.setDataHandler(new DataHandler(fds));
				messageBodyPart.setHeader("Content-ID", String.format("<adapter%d>", count));
				multipart.addBodyPart(messageBodyPart);
				count++;
			}

			// put everything together
			msg.setContent(multipart);
			logger.log(Level.FINE, "Message successfully build!");

			Transport.send(msg);

			logger.log(Level.FINE, "E-Mail successfully sent!");
			return true;
		} catch (Exception e) {
			logger.log(Level.WARNING, "E-Mail NOT sent!");
			e.printStackTrace();
			return false;
		}
	}
}
