package space.schober.adapternachten;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class App {
	static Logger logger = Logger.getAnonymousLogger();
	private static final Level LOG_LEVEL = Level.FINEST;

	public static void main(String[] args) {
		logger.setLevel(LOG_LEVEL);

		ConsoleHandler handler = new ConsoleHandler();
		handler.setLevel(LOG_LEVEL);
		logger.addHandler(handler);

		Set<Person> persons = ReaderWriter.readList("src/main/java/io/default.csv");
		Person.addLinkedPersons(persons);
		Email email = new Email("adapternachten@gmail.com", args[0]);

		try (BufferedReader br = new BufferedReader(new FileReader("src/main/java/io/email/index.html"));) {

			StringBuilder stb = new StringBuilder();

			while (br.ready()) {
				stb.append(br.readLine());
				stb.append("\n");
			}

			ArrayList<String> adapter = new ArrayList<>();
			for (int i = 0; i < 8; i++) {
				adapter.add(String.format("Adapter/%d.png", i));
			}

			for (Person person : persons) {
				email.sendEmail(person, stb.toString(), "src/main/java/io/email/src/", adapter);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
